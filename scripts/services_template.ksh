print_services_template() {
print "Services config file has to contain two values per line
  1.Name of url to call
  2.Max retry counter
Default retry counter is not used for passed services, so 2nd point is obligatory.
E.g.
  localhost|20
  www.google.com|3
";
}

print_services_template
