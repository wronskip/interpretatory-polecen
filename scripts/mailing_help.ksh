#!/usr/local/bin/ksh93

print_mailing_help() {
print 'Mail is sent via local server designed for this purpose (exposed on 127.0.0.1:8080/api/mailer)
Recipients of message are kept in ./config/service-monitoring.mails.json as json array i.e.
["email@address1.com", "email@address2.com"]
Subject and body of mail are kept in ./config/service-monitoring.message
First line is Subject
Second and rest are Body
You can use variables in subject and body that are automatically replaced with values:
  _srv_     =     name of now working service
  _date_    =     now date in default format
'
}

print_mailing_help
