#!/usr/local/bin/ksh93

print_help() {
print "Service monitoring script v 1.0.0
Program that monitors availability of web services and notify about errors
Warning: this version uses standalone mailing application with hardcoded mail user.
Options:
        -h                    Shows help
        -x                    Shows format for config file with services
        -m                    Shows format for mailing configuration
        -c                    Print logs to console
        -p                    Enable spamming mode i.e. resets failure counter
                              and tests not-working service again.
                              Default: disabled, once service is found not working,
                              only one mail is sent and service is not beeing tested
                              anymore
        -f [file name]        Loads services from passed file.
                              See -x
        -s [time in seconds]  Sets sleep time between each services check.
                              Default: 60 seconds
        -v [url]              Adds service to test with default retry counter (see -r)
        -r [number]           Sets default retry counter for all services passed with -v,
                              Default: 5 times
";
}

print_help
