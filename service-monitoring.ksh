#!/usr/local/bin/ksh93

#Variable that holds all services that should be tested in script
set -A services
set -A maxretry
set -A failures
set -A arg_services

#Config variables
default_sleep_time=60
default_retry_count=5
stop_spam=true
print_to_console=false
log_file='./logs/sm.logs'
mail_file='./config/service-monitoring.message'

#init failures with services
init_failures() {
  for i in "${services[@]}"
    do
      :
      failures+=(["$i"]=0);
    done
}

#Log mesage to file and console based on print_to_console flag
#$1 message to log
log_msg() {
  now=$( date )
  user=$( whoami )
  echo "$now     $user     $1" >> "$log_file";
  if [ "$stop_spam" = "true" ]
  then
    print "$now     $user     $1"
  fi
}

# ========================================#
#     functions used in getopts
# ========================================#

print_help() {
  /usr/local/bin/ksh93 ./scripts/help.ksh
  exit
}

print_services_template() {
  /usr/local/bin/ksh93 ./scripts/services_template.ksh
  exit
}

print_mailing_help() {
  /usr/local/bin/ksh93 ./scripts/mailing_help.ksh
  exit
}

#Checks if argument is number
# $1 value to test
# $2 message to print if value is not number
test_number() {
  if [[ "$1" != ?(-)+([0-9]) ]]; then
      print "$2"
      log_msg "$2"
      exit;
  fi
}

#$1 - name of config file
load_config_from_file() {
  i=0
  while read -r line
    do
      IFS='|' read srv retry <<< "$line"
      test_number "$retry" "Error: Value ${service_opts[1]} can not be interpreted as retry counter for service ${service_opts[0]}"
      services+=("$srv");
      maxretry+=(["$srv"]=$retry);
      ((i++))
    done < "$1"
    init_failures
}

init_arg_services() {
  for arg_srv in "${arg_services[@]}"
    do
      :
      services+=("$arg_srv")
      failures["$arg_srv"]="0"
      maxretry["$arg_srv"]=$default_retry_count
    done
}

#processing script options

while getopts hxmpf:s:r:v:c option
  do
    case $option in
      h)    print_help;;
      x)    print_services_template;;
      m)    print_mailing_help;;
      p)    stop_spam=false;;
      c)    print_to_console=true;;
      f)    load_config_from_file $OPTARG;;
      s)    default_sleep_time=$OPTARG;;
      r)    test_number $OPTARG "Error: Value $OPTARG can not be interpreted as default retry counter"
            default_retry_count=$OPTARG;;
      v)    arg_services+=("$OPTARG");;
      \?)	  print "Error: Uknown option passed. Exiting..."
            exit;;
    esac
done
init_arg_services

#Handles spam scenario based on flag stop_spam
#$1 - service to handle
handle_spam() {
  if [ "$stop_spam" = "true" ]
  then
    log_msg "Service $1 not working".
  else
    log_msg "Service $1 failures counter set to zero after receiving max retries limit"
    (( failures[$1]=0 ))
  fi
}


#Checks if service is available with ping
ping_test() {
  ping -q -c5 $1 > /dev/null 2> /dev/null

  if [ $? -eq 0 ];
  then
	   echo true
   else
     echo false
  fi
}

#Iterates over services and invokes ping_test on them
test_services() {
  for i in "${services[@]}"
    do
      :
      running=$( ping_test $i )
      if [ "$running" = "false" ]
        then
          (( failures[$i]++ ))
      fi
    done
}

send_mail() {
  sender=$( whoami )
  now_time=$( date )
  recipient=`cat ./config/service-monitoring.mails.json`
  subject=`head -n 1 $mail_file`
  subject="${subject/_srv_/$1}"
  subject="${subject/_time_/$now_time}"
  body=`tail -n +2 $mail_file`
  body="${body/_srv_/$1}"
  body="${body/_time_/$now_time}"
  json="{\"sender\":\"$sender\", \"recipients\": $recipient , \"subject\":\"$subject\", \"body\":\"$body\"}"
  log_msg "Sending message to $recipient with Subject: $subject and Body: $body"
  curl 127.0.0.1:8080/api/mailer -X POST -d "$json" -H "Content-Type: application/json" > /dev/null 2> /dev/null
}

# main body of script
# infinite loop that
#   1. tests services
#   2. sends mails if failures are equals retry_count
#   3. sleeps specified time before next execution
main_body() {
  touch $log_file
  log_msg "Service monitoring script started with PID : $$"
  while :
  do
    log_msg "Testing services"
    test_services
    log_msg "Services tested"

    for s in "${services[@]}"
    do
      :
      first="${maxretry[${s}]}"
      second="${failures[${s}]}"
      log_msg "service: $s maxretry: $first failures: $second"
      if [ "$first" == "$second" ]
      then
        send_mail $s
        handle_spam $s
      fi
    done
    log_msg "Sleeping for $default_sleep_time..."
    sleep $default_sleep_time
  done
}

main_body

exit 0;
